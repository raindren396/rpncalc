/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
 */
package com.rpncalc.dao;

import java.util.List;

import com.rpncalc.model.ApiAudit;

public interface ApiAuditDAO {

    public ApiAudit findApiAudit(Integer id);
    
    public void save(ApiAudit p);

    public List<ApiAudit> viewAudits();
}
