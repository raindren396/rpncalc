/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
 */
package com.rpncalc.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.rpncalc.model.ApiAudit;
import com.rpncalc.persistence.CrudService;

@Repository
public class ApiAuditDAOImpl implements ApiAuditDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiAuditDAOImpl.class);

    private CrudService crudService;

    public ApiAuditDAOImpl() {
    }

    public ApiAuditDAOImpl(CrudService crudService) {
        this.crudService = crudService; 
    }

    @Override
    public ApiAudit findApiAudit(Integer id) {
        return crudService.findById(ApiAudit.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ApiAudit> viewAudits() {
        return crudService.findAll(ApiAudit.class);
    }

    @Override
    public void save(ApiAudit audit) {
        crudService.persist(audit);
        LOGGER.info("Audit saved successfully");
    }
}
