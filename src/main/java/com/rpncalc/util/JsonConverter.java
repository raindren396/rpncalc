package com.rpncalc.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonConverter.class);

    public static String asJsonString(final Object obj) {

        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            LOGGER.error("Error converting to json: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
