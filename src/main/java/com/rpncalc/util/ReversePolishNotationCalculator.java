/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
 */
package com.rpncalc.util;

import java.util.Stack;

import com.rpncalc.exception.CalculatorException;

public class ReversePolishNotationCalculator {

    public static Double calcRevPol(String expression) {
        Stack<Double> st = new Stack<>();
        double a;
        double b;

        if (expression == null || "".equals(expression)) {
            throw new CalculatorException("Null or empty expression.");
        }

        String[] exp = expression.split(" ");

        for (String s : exp) {
            switch (s) {
                case "+":
                    checkStack(st);
                    a = st.pop();
                    b = st.pop();
                    st.push(b + a);
                    break;
                case "-":
                    checkStack(st);
                    a = st.pop();
                    b = st.pop();
                    st.push(b - a);
                    break;
                case "*":
                    checkStack(st);
                    a = st.pop();
                    b = st.pop();
                    st.push(b * a);
                    break;
                case "/":
                    checkStack(st);
                    a = st.pop();
                    b = st.pop();
                    st.push(b / a);
                    break;
                default:
                    try {
                        st.push(Double.parseDouble(s));
                    } catch (NumberFormatException e) {
                        throw new CalculatorException("Expression includes invalid values.");
                    }
            }
        }

        if (st.size() > 1) {
            throw new CalculatorException("Invalid expression.");
        }

        return st.pop();
    }
    
    private static Stack checkStack(Stack st){
        if (st.size() < 2) {
            throw new CalculatorException("Invalid expression.");
        }
        return st;
    }
}
