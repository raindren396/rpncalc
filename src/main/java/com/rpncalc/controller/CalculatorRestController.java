/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/

package com.rpncalc.controller;

import com.rpncalc.util.ReversePolishNotationCalculator;
import com.rpncalc.dto.CalculatorRequest;
import com.rpncalc.dto.CalculatorResponse;
import com.rpncalc.dto.ErrorMessageResponse;
import com.rpncalc.exception.CalculatorException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorRestController {

    @RequestMapping(value = "/calculate", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public CalculatorResponse calculateExpression(@RequestBody CalculatorRequest request) {
        return new CalculatorResponse(ReversePolishNotationCalculator.calcRevPol(request.getExpression()));
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CalculatorException.class)
    public ErrorMessageResponse handleCalculatorException(CalculatorException ex) {
        return new ErrorMessageResponse("Validation error", ex.getMessage());
    }

}
