/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/

package com.rpncalc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rpncalc.model.ApiAudit;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import com.rpncalc.service.ApiAuditService;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/api_audits")
public class ApiAuditRestController {

    private ApiAuditService apiAuditService;

    @Autowired(required = true)
    @Qualifier(value = "apiAuditService")
    public void setApiAuditService(ApiAuditService apiAuditService) {
        this.apiAuditService = apiAuditService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ApiAudit> listApiAudits() {
        return this.apiAuditService.viewAudits();
    }    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ApiAudit findApiAudit(@PathVariable Integer id) {
        return this.apiAuditService.findApiAudit(id);
    }
}
