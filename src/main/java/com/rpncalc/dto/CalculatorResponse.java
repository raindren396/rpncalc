/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/

package com.rpncalc.dto;

public class CalculatorResponse {
	
	private Double result;

	public CalculatorResponse() {
	}

	public CalculatorResponse(Double result) {
		this.result = result;
	}

	public Double getResult() {
		return result;
	}

	@SuppressWarnings("unused")
	private void setResult(Double result) {
		this.result = result;
	}
}
