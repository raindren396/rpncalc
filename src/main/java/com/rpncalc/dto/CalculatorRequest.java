/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/

package com.rpncalc.dto;

public class CalculatorRequest {

	private String expression;

	public CalculatorRequest() {
	}

	public CalculatorRequest(String expression) {
		this.expression = expression;
	}

	public String getExpression() {
		return expression;
	}

	@SuppressWarnings("unused")
	private void setExpression(String expression) {
		this.expression = expression;
	}
}
