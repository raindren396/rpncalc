/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/

package com.rpncalc.exception;

public class CalculatorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String message;

	public CalculatorException(String message) {
		this.message = message;
	}

        @Override
	public String getMessage() {
		return message;
	}

	@SuppressWarnings("unused")
	private void setMessage(String message) {
		this.message = message;
	}
}
