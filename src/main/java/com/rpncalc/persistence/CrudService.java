package com.rpncalc.persistence;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CrudService {

    private Session session;

    public CrudService() {
    }

    public CrudService(SessionFactory sessionFactory) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
    }

    public <T extends Persistable> T persist(T entity) {
        session.persist(entity);
        session.flush();
        session.refresh(entity);
        return entity;
    }

    public <T extends Persistable> List<T> findAll(Class<T> type) {
        Query query = session.createQuery(" from " + type.getSimpleName());
        return query.list();
    }

    public <T extends Persistable, ID> T findById(Class<T> type, ID id) {
        Query query = session.createQuery(" from " + type.getSimpleName() + " where id = :id");
        query.setParameter("id", id);

        List<T> results = query.list();
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }
}
