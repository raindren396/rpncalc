/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
 */
package com.rpncalc.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rpncalc.model.ApiAudit;
import com.rpncalc.dao.ApiAuditDAO;
import com.rpncalc.dao.ApiAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ApiAuditServiceImpl implements ApiAuditService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiAuditDAOImpl.class);
    
    private ApiAuditDAO apiAuditDAO;

    public void setApiAuditDAO(ApiAuditDAO apiAuditDAO) {
        this.apiAuditDAO = apiAuditDAO;
    }

    public ApiAudit findApiAudit(Integer id) {
        if(id == null){
            //TODO: throw exception  
            return null;
        }
        ApiAudit apiAudit = apiAuditDAO.findApiAudit(id);
        if (apiAudit == null){
            //TODO: throw exception
            return null;
        }
        return apiAudit;
    }

    @Override
    @Transactional
    public List<ApiAudit> viewAudits() {
        return this.apiAuditDAO.viewAudits();
    }

    @Override
    @Transactional
    public void save(ApiAudit p) {
        if(p == null){
            //TODO: throw exception
            LOGGER.error("Can not save a null api audit.");
            return;
        }
        this.apiAuditDAO.save(p);
    }
}
