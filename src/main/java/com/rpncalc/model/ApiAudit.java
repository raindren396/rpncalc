/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
 */
package com.rpncalc.model;

import com.rpncalc.persistence.Persistable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "api_audit")
public class ApiAudit extends Persistable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "http_method")
    private String httpMethod;

    @Column(name = "end_point")
    private String endpoint;

    @Column(name = "request_body")
    private String requestBody;

    @Column(name = "response_body")
    private String responseBody;

    @Column(name = "response_status")
    private Integer responseStatusCode;

    public ApiAudit() {
    }

    public ApiAudit(String httpMethod, String endpoint, String requestBody, String responseBody,
            Integer responseStatusCode) {
        this.httpMethod = httpMethod;
        this.endpoint = endpoint;
        this.requestBody = requestBody;
        this.responseBody = responseBody;
        this.responseStatusCode = responseStatusCode;
    }

    public ApiAudit(int id, String httpMethod, String endpoint, String requestBody, String responseBody, Integer responseStatusCode) {
        this.id = id;
        this.httpMethod = httpMethod;
        this.endpoint = endpoint;
        this.requestBody = requestBody;
        this.responseBody = responseBody;
        this.responseStatusCode = responseStatusCode;
    }

    public int getId() {
        return id;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public Integer getResponseStatusCode() {
        return responseStatusCode;
    }

    @SuppressWarnings("unused")
    private void setId(int id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    private void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    @SuppressWarnings("unused")
    private void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    @SuppressWarnings("unused")
    private void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    @SuppressWarnings("unused")
    private void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    @SuppressWarnings("unused")
    private void setResponseStatusCode(Integer responseStatusCode) {
        this.responseStatusCode = responseStatusCode;
    }
}
