/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/

package com.rpncalc.filter;

import com.rpncalc.util.HttpResponseWrapper;
import com.rpncalc.util.HttpRequestWrapper;
import com.rpncalc.model.ApiAudit;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.rpncalc.service.ApiAuditService;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ApiAuditFilter implements Filter {

    public static final Set<String> INCLUDE_PATHS = new HashSet<>(Arrays.asList("/calculate"));

    private ApiAuditService apiAuditService;
    private FilterConfig filterConfig;
    
    @Autowired(required = true)
    @Qualifier(value = "apiAuditService")
    public void setApiAuditService(ApiAuditService apiAuditService) {
        this.apiAuditService = apiAuditService;
    }

    @Override
    public void destroy() {
        filterConfig = null;
        apiAuditService = null;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpRequestWrapper httpRequest = new HttpRequestWrapper((HttpServletRequest) request);
        HttpResponseWrapper httpResponse = new HttpResponseWrapper((HttpServletResponse) response);

        String path = httpRequest.getPathInfo();

        if (!INCLUDE_PATHS.contains(path)) {
            chain.doFilter(httpRequest, httpResponse);
        } else {
            String httpMethod = httpRequest.getMethod();
            String requestBody = httpRequest.getBody();

            chain.doFilter(httpRequest, httpResponse);

            String responseBody = new String(httpResponse.getCopy(), httpResponse.getCharacterEncoding());
            Integer responseStatusCode = httpResponse.getStatus();

            if (apiAuditService == null) {
                WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
                apiAuditService = (ApiAuditService) springContext.getBean("apiAuditService");
            }

            apiAuditService.save(new ApiAudit(httpMethod, path, requestBody, responseBody, responseStatusCode));
        }
    }
}
