package com.rpncalc.test;

import com.rpncalc.controller.ApiAuditRestController;
import com.rpncalc.filter.ApiAuditFilter;
import com.rpncalc.filter.CORSFilter;
import com.rpncalc.model.ApiAudit;
import com.rpncalc.service.ApiAuditService;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

public class ApiAuditRestControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ApiAuditService apiAuditService;

    @InjectMocks
    private ApiAuditRestController controller;

    @InjectMocks
    private ApiAuditFilter apiAuditFilter;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .addFilters(new CORSFilter(), apiAuditFilter)
                .build();
    }

    @Test
    public void test_listApiAudits_success() throws Exception {
        final List<ApiAudit> AUDITS = Arrays.asList(
                new ApiAudit(1, "httpMethod1", "endpoint1", "requestBody1", "responseBody1", 123),
                new ApiAudit(2, "httpMethod2", "endpoint2", "requestBody2", "responseBody2", 321));
        
        when(apiAuditService.viewAudits()).thenReturn(AUDITS);

        mockMvc.perform(get("/api_audits"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].httpMethod", is("httpMethod1")))
                .andExpect(jsonPath("$[0].endpoint", is("endpoint1")))
                .andExpect(jsonPath("$[0].requestBody", is("requestBody1")))
                .andExpect(jsonPath("$[0].responseBody", is("responseBody1")))
                .andExpect(jsonPath("$[0].responseStatusCode", is(123)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].httpMethod", is("httpMethod2")))
                .andExpect(jsonPath("$[1].endpoint", is("endpoint2")))
                .andExpect(jsonPath("$[1].requestBody", is("requestBody2")))
                .andExpect(jsonPath("$[1].responseBody", is("responseBody2")))
                .andExpect(jsonPath("$[1].responseStatusCode", is(321)));
    }
    
//below code is from maletsiri which should help the above

//    classUnderTest = new JobService();
//        classUnderTest.setJobDao(mockJobDao);
//        classUnderTest.setJobHelper(mockJobHelper);
//        classUnderTest.setEventHandler(mockEventHandler);
//
//        when(mockEventHandler.raiseEvent(any(EventDto.class), any(Boolean.class))).thenReturn(new EventResponse("Msg"));
}
