/*
    Reverse Polish Notation Calculator RESTful Webservice
    by Raindren Padayachee 
*/
package com.rpncalc.test;

import com.rpncalc.controller.CalculatorRestController;
import com.rpncalc.dto.CalculatorRequest;
import com.rpncalc.filter.ApiAuditFilter;
import com.rpncalc.filter.CORSFilter;
import com.rpncalc.service.ApiAuditService;
import com.rpncalc.util.JsonConverter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.core.Is.is;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations.Mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CalculatorRestControllerTest {
    private final String URL = "/calculate";
    private final String VALIDATION_ERROR = "Validation error";
    
    private MockMvc mockMvc;

    @InjectMocks
    private CalculatorRestController controller;

    @Mock
    private ApiAuditService apiAuditService;
    
    @InjectMocks
    private ApiAuditFilter apiAuditFilter;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .addFilters(new CORSFilter(), apiAuditFilter)
                .build();
    }

    @Test
    public void test_calculator_success() throws Exception {
        mockMvc.perform(
                post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonConverter.asJsonString(new CalculatorRequest("1 1 +"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.result", is(2.0)));
    }
    
    
    @Test
    public void test_calculator_emptyExpression() throws Exception {
        mockMvc.perform(
                post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonConverter.asJsonString(new CalculatorRequest(""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.errorType", is(VALIDATION_ERROR)))
                .andExpect(jsonPath("$.errorMessage", is("Null or empty expression.")));
    }
    
    @Test
    public void test_calculator_nullExpression() throws Exception {
        mockMvc.perform(
                post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonConverter.asJsonString(new CalculatorRequest(null))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.errorType", is(VALIDATION_ERROR)))
                .andExpect(jsonPath("$.errorMessage", is("Null or empty expression.")));
    }
    
    @Test
    public void test_calculator_invalidValueExpression() throws Exception {
        mockMvc.perform(
                post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonConverter.asJsonString(new CalculatorRequest("1 a +"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.errorType", is(VALIDATION_ERROR)))
                .andExpect(jsonPath("$.errorMessage", is("Expression includes invalid values.")));
    }
    
    @Test
    public void test_calculator_invalidExpression() throws Exception {
        mockMvc.perform(
                post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonConverter.asJsonString(new CalculatorRequest("1 +"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.errorType", is(VALIDATION_ERROR)))
                .andExpect(jsonPath("$.errorMessage", is("Invalid expression.")));
    }
}
