# Reverse Polish Notation Calculator RESTful Webservice #

## *by Raindren Padayachee* ##
*05/09/2016*
____
____

### *Summary:* ###

This project is a RESTful webservice build in Spring, which features a reverse polish notation calculator.
_____

### *Description:* ###

This RESTful webservice is build on the spring framework. Dependence injection is achived using both annotation and xml based configuration. Hibernate and mySql is used to persist the requests and responses through a servlet filter. It accepts and produces all request and response bodies as json, and achieve this using Jackson databind. It is unit tested with junit and mockito and currently only has two endpoints. One to calculate a reverse polish notation expression, and another to list all api requests and responses so far. It was build in Netbeans on Ubuntu and runs in Tomcat 7, and uses Maven for dependency management.
____

### *Setup:* ###
*
Requirements:* 

* Java 8
* Tomcat 7
* mySql
* Maven

*Steps:*

1. Clone the project, or download source code
1. In mySql create a database named: calculator_db
1. Run the dbScript.sql script in the project directory root, on this new database
1. Run the following command in the project directory root: mvn clean install
1. A directory called target will be created in the project directory root. 
1. Copy the RPNCalc.war file from this new directory, to the tomcat webapps directory.
1. Start Tomcat.
1. The webservice should now be up ane running.
____

### *API Endpoints:* ###

*Calculate a reverse polish notation expression:*

* Http method: POST
* Url: /rpncalc/calculate
* Request header: Content-type:application/json
* Example of request body: 
```
#!json

{"expression":"1 2 + 6 -"}
```

* Example of successful response body: 
```
#!json

{"result":"-2.5"}
```


*View api request and response audit:*

* Http method: GET
* Url: /rpncalc/api_audits
* Request header: Content-type:application/json
* Example of successful response body: 

```
#!json

[{
    "id": 1,
    "httpMethod": "POST",
    "endpoint": "/calculate",
    "requestBody": "{    \"expression\":\"1 1 +\"}",
    "responseBody": "{\"result\":2}",
    "responseStatusCode": 200
  },
  {
    "id": 2,
    "httpMethod": "POST",
    "endpoint": "/calculate",
    "requestBody": "{    \"expression\":\"1 1 +\"}",
    "responseBody": "{\"result\":2}",
    "responseStatusCode": 200
  }]
```