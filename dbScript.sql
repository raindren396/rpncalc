CREATE TABLE `api_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `http_method` varchar(10) DEFAULT NULL,
  `end_point` varchar(45) DEFAULT NULL,
  `request_body` text,
  `response_body` text,
  `response_status` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;